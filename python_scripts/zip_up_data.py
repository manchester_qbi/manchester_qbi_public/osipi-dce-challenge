#Core python imports
import os
import sys
from shutil import copy, make_archive

#External libraries
import nibabel as nib
import numpy as np

#Local imports
from helpers import process_datasets, cls

#---------------------------------------------------------------
def zipup_subject_visit(visit_dir: str, ktrans_dir:str):
    '''
    '''
    dirs = os.path.normpath(visit_dir).split(os.path.sep)
    subject = dirs[-2]
    visit = dirs[-1]
    path_src = os.path.join(visit_dir, 'madym_output', 'ETM_final', 'Ktrans.nii.gz')
    path_dst = os.path.join(ktrans_dir, f'{subject}_{visit}_Ktrans.nii.gz')

    print(f'Copying Ktrans from {visit_dir} to {path_dst}')
    copy(path_src, path_dst)
    return os.path.exists(path_dst)

#---------------------------------------------------------------
def print_summary_stats(visit_dir, fid):
    dirs = os.path.normpath(visit_dir).split(os.path.sep)
    subject = dirs[-2]
    visit = dirs[-1][-1]

    mask_pth = os.path.join(visit_dir, 'mask', 'brain_mask.nii.gz')
    T1_pth = os.path.join(visit_dir, 'madym_output', 'T1', 'T1.nii.gz')
    Ktrans_pth = os.path.join(visit_dir, 'madym_output', 'ETM_final', 'Ktrans.nii.gz')

    mask = nib.load(mask_pth).get_fdata() > 0
    T1 = nib.load(T1_pth).get_fdata()
    Ktrans = nib.load(Ktrans_pth).get_fdata()

    T1_med = np.nanmedian(T1[mask])
    Ktrans_med = np.nanmedian(Ktrans[mask])
    print(f'{subject}, {visit}, {T1_med:5.4f}, {Ktrans_med:5.4f}', file=fid)
    return True

#---------------------------------------------------------------
def save_summary_stats(root_dir, fid):
    print('Subject, Visit, T1 median, Ktrans median', file=fid)
    process_datasets(
      print_summary_stats, root_dir, fid=fid)

#---------------------------------------------------------------
#---------------------------------------------------------------
def zipup_data(root_dir:str):
    '''
    Zip-up the Ktrans maps for each subject-visit into an archive file
    for submission
    '''
    print(f'Zipping up data in {root_dir}')
    ktrans_dir = os.path.join(root_dir, 'OSIPI_DCE_challenge_QBI_Manchester')
    os.makedirs(ktrans_dir, exist_ok=True)

    #Copy over the Ktrans maps for each subject/visit
    success = process_datasets(
      zipup_subject_visit, root_dir, ktrans_dir=ktrans_dir)

    #Copy the SOP instructions
    sop_src = os.path.abspath('madym_sop.pdf')
    sop_dst = os.path.join(ktrans_dir, 'madym_sop.pdf')
    copy(sop_src, sop_dst)

    #Zip into a single archive for submission
    make_archive(ktrans_dir, 'zip', ktrans_dir)

    #Make a summary stats file
    with open(os.path.join(root_dir, 'summary_stats.csv'), 'wt') as fid:
        save_summary_stats(root_dir, fid)

    if success:
        cls()
        print('********************************************************\n')
        print('OSIPI DCE-MRI challenge: data zipped up successfully.')
        print(f'The final zipped submission file is {ktrans_dir}.zip')
        print('Challenge complete!')
        print('\n********************************************************')

#---------------------------------------------------------------
#---------------------------------------------------------------
if __name__ == '__main__':
    root_dir = ''

    if len(sys.argv) > 1:
        root_dir = sys.argv[1]
    
    zipup_data(root_dir)
        