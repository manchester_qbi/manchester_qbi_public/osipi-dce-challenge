#Core python imports
import os
import sys
from warnings import catch_warnings

#External imports
import numpy as np
import nibabel as nib

#Madym imports
from QbiMadym import madym_T1, madym_DCE

#Local imports
from helpers import process_datasets, cls

#---------------------------------------------------------------
def fit_ETM_BATs(visit_dir:str, config_dir:str):
  '''
  Fit the extended-Tofts model at a series of bolus arrival times (BAT),
  selecting the parameters from the BAT the provides the lowest fit
  residual
  '''
  BATs = np.linspace(0, 19.2, 9)/60
  map_names = ['Ktrans', 'v_e', 'v_p', 'tau_a']
  dyn_mean = nib.load(os.path.join(visit_dir, 'dynamic', 'dyn__mean.nii.gz'))
  dims = dyn_mean.get_fdata().shape[0:3]

  #Pre-allocate a set of empty 3D arrays for the final parameter values
  final_maps = dict()
  for map_name in map_names:
    final_maps[map_name] = np.zeros(dims)

  #Set residuals to infinite initially
  residuals = np.full(dims, np.Inf)

  for i_B, BAT in enumerate(BATs):
    #Call Madym to run the ETM fit for this BAT
    madym_DCE.run(
      config_file = os.path.join(config_dir, 'madym_ETM_config.txt'),
      audit_dir = os.path.join(visit_dir, 'audit_logs'),
      init_params = [0, 0, 0, BAT],
      output_dir = f'_BAT{i_B:02d}', 
      working_directory = visit_dir)

    #Load in the residuals for this BAT and get mask of voxels with
    #improved fit
    BAT_output_dir = os.path.join(visit_dir, 'madym_output', f'ETM_BAT{i_B:02d}')
    residuals_path = os.path.join(BAT_output_dir, 'residuals.nii.gz')
    BAT_residuals = np.squeeze(nib.load(residuals_path).get_fdata())
    swap = BAT_residuals < residuals
    residuals[swap] = BAT_residuals[swap]

    #Load in each parameter map, and swap the voxels with improved fit
    # into the final maps 
    for map_name in map_names:
      map_path = os.path.join(BAT_output_dir, map_name + '.nii.gz')
      map = np.squeeze(nib.load(map_path).get_fdata())
      final_maps[map_name][swap] = map[swap]

  #Save the final set of maps
  final_output_dir = os.path.join(visit_dir, 'madym_output', 'ETM_final')  
  os.makedirs(final_output_dir, exist_ok=True)

  for map_name in map_names:
    map_path = os.path.join(final_output_dir, map_name + '.nii.gz')
    map_ni = nib.Nifti1Image(
        final_maps[map_name], np.eye(4))
    map_ni.header['pixdim'] = dyn_mean.header['pixdim']
        
    nib.save(map_ni, map_path)

  return os.path.exists(map_path)

#---------------------------------------------------------------
def analyse_subject_visit(visit_dir: str, config_dir:str, steps = [1,2]):
    '''
    Run complete analysis for a single subject visit, applying the following steps:
    1. Baseline T1 mapping of VFA data
    2. Fit the ETM to DCE data at a series of bolus arrival times
    '''

    #1. Baseline T1 mapping of VFA data
    if 1 in steps:
        madym_T1.run(
          config_file = os.path.join(config_dir, 'madym_T1_config.txt'), 
          audit_dir = os.path.join(visit_dir, 'audit_logs'),
          working_directory = visit_dir)

    #2. Fit the ETM to DCE data
    success = True
    if 2 in steps:
      try:
        success = fit_ETM_BATs(visit_dir, config_dir)
      except:
        success = False

    return success


#---------------------------------------------------------------
#---------------------------------------------------------------
def analyse_data(root_dir:str, config_dir:str):
    '''
    '''
    print(f'Analysing data in {root_dir}, using config files in {config_dir}')

    success = process_datasets(
      analyse_subject_visit, root_dir, config_dir=config_dir, steps=[1,2])

    if success:
        cls()
        print('********************************************************\n')
        print('OSIPI DCE-MRI challenge: data analysed successfully.')
        print('\n********************************************************')

#---------------------------------------------------------------
#---------------------------------------------------------------
if __name__ == '__main__':
    root_dir = ''
    config_dir = os.path.abspath('madym_config')

    if len(sys.argv) > 1:
        root_dir = sys.argv[1]
    if len(sys.argv) > 2:
        config_dir = sys.argv[2]
    
    analyse_data(root_dir, config_dir)
        