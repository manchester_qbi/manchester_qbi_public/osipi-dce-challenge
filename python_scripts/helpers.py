import os

def process_datasets(process_fun, root_dir, **kwargs):
    datasets = ['Clinical', 'Synthetic']

    num_subjects = {
        'Clinical': 8,
        'Synthetic': 2 
        }

    success = True
    for dataset in datasets:
        for subject in range(1,num_subjects[dataset]+1):
            for visit in  [1,2]:
                
                visit_dir = os.path.join(root_dir, 
                    f'{dataset}_Data', 
                    f'{dataset}_P{subject}', f'Visit{visit}')

                #Sanity check we've set the visit paths correctly
                if not os.path.exists(visit_dir):
                    raise RuntimeError(
                        f'Error {visit_dir} is missing: processing failed.\n'
                        f' Check input data_dir ({root_dir}) is correct')

                #Analyse the data
                success = success and process_fun(visit_dir, **kwargs)

    return success

def cls():
    os.system('cls' if os.name=='nt' else 'clear')