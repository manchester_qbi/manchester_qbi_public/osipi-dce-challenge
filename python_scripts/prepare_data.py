#Python core imports
import sys
import os
import glob

#External imports
import numpy as np
import skimage.measure as skm
import nibabel as nib

#Madym imports
from QbiMadym import madym_DicomConvert

#Local imports
from helpers import process_datasets, cls

#---------------------------------------------------------------
def get_dicom_dce_dir(visit_dir:str):
    '''
    Get the name of the DICOM folder containing the DCE data
    (VFA + dynamic time-series)
    '''
    dicom_dirs = glob.glob(os.path.join(visit_dir, '*BRAIN*'))
    for dicom_dir in dicom_dirs:
        n_series = len(next(os.walk(dicom_dir))[1])
        if n_series >= 7:
            break
    return os.path.basename(dicom_dir), n_series

#---------------------------------------------------------------
def make_brain_mask(visit_dir:str, thresh=30):
    '''
    Given a dynamic time-series of images, make an approximate
    ROI mask of the brain.

    This is a quick and dirty function that uses a hard threshold
    on the mean dynamic signal. It certainly does not give an
    accurate segmentation, but should be sufficient to remove most
    of the background so we don't waste time processing unnecessary voxels.
    It is designed to be a loose mask, as the most important factor is not
    to remove voxels that the OSIPI challenge setters have included in the
    random selections for the evaluation.
    '''
    #Load dynamic mean and threshold
    dyn_mean = nib.load(os.path.join(visit_dir, 'dynamic', 'dyn__mean.nii.gz'))
    brain_mask = dyn_mean.get_fdata() > thresh

    #Only keep largest connected component
    mask_labels = skm.label(brain_mask[:,:,:,0], connectivity=1)
    n_labels = np.max(mask_labels)
    label_sz = np.empty((n_labels))
    for label in range(n_labels):
        label_sz[label] = np.sum(mask_labels==label+1)
    max_label = np.argmax(label_sz)+1
    brain_mask = mask_labels == max_label

    #Convert mask array back to nifti image object
    brain_mask_ni = nib.Nifti1Image(
        brain_mask.astype(np.uint8), np.eye(4))
    brain_mask_ni.header['pixdim'] = dyn_mean.header['pixdim']

    #Save mask
    mask_dir = os.path.join(visit_dir, 'mask')
    mask_path = os.path.join(mask_dir, 'brain_mask.nii.gz')
    os.makedirs(mask_dir, exist_ok=True)
    nib.save(brain_mask_ni, mask_path)
    return os.path.exists(mask_path)

#---------------------------------------------------------------
def prepare_subject_visit(visit_dir: str, config_dir:str, steps = [1,2,3]):
    '''
    Run complete analysis for a single subject visit, applying the following steps:
    1. Sort the DICOM data
    2. Convert the DICOM data to NIFTI volumes for Madym analysis
    3. Make ROI mask by thresholding temporal mean of DCE series
    4. Baseline T1 mapping of VFA data
    5. Fit the ETM to DCE data
    '''

    #Pre-check required for steps 1+2
    dicom_dce_dir,n_series = get_dicom_dce_dir(visit_dir)

    if (1 in steps) or (2 in steps):
        if n_series < 7:
            raise RuntimeError('Could not find data for DCE series')
        elif n_series == 7:
            t1_series = [1, 2, 3, 4, 5, 6]
            dyn_series = 7
        elif n_series == 8:
            t1_series = None
            dyn_series = None
        else:
            raise RuntimeError('Unexpected number of DCE series found')

    #1. Sort DICOM data
    if 1 in steps:
        madym_DicomConvert.run(
            config_file = os.path.join(config_dir, 'madym_dicom_sort_config.txt'),
            audit_dir = os.path.join(visit_dir, 'audit_logs'),
            dicom_dir = dicom_dce_dir, 
            working_directory = visit_dir)

    #2. Convert the DICOM data to NIFTI volumes for Madym analysis
    if 2 in steps:
        madym_DicomConvert.run(
            config_file = os.path.join(config_dir, 'madym_dicom_make_config.txt'),
            audit_dir = os.path.join(visit_dir, 'audit_logs'),
            dicom_dir = dicom_dce_dir, 
            T1_input_series = t1_series,
            dyn_series = dyn_series,
            working_directory = visit_dir)

    #3. Make ROI mask by thresholding temporal mean of DCE series
    success = True
    if 3 in steps:
        success = make_brain_mask(visit_dir)

    return success

#---------------------------------------------------------------
#---------------------------------------------------------------
def prepare_data(root_dir:str, config_dir:str):
    '''
    '''
    print(f'Preparing data in {root_dir}, using config files in {config_dir}')

    #Apply the main data preparation, converting DICOM data to NIFTI images
    #then creating an ROI mask of the brain
    success = process_datasets(
      prepare_subject_visit, root_dir, config_dir=config_dir, steps=[1,2,3])

    if success:
        cls()
        print('********************************************************\n')
        print('OSIPI DCE-MRI challenge: data prepared successfully.')
        print('\n********************************************************')

#---------------------------------------------------------------
#---------------------------------------------------------------
if __name__ == '__main__':
    root_dir = ''
    config_dir = os.path.abspath('madym_config')

    if len(sys.argv) > 1:
        root_dir = sys.argv[1]
    if len(sys.argv) > 2:
        config_dir = sys.argv[2]
    
    prepare_data(root_dir, config_dir)

