# This script is used to showcase the QBI-manchester submission to the OSIPI DCE challenge

#%%
#1) we need to unzip the data
import zipfile
import os
import glob
import nibabel
import matplotlib.pyplot as plt
import numpy as np
import skimage.measure as skm

#%%
root_dir = 'C:\\isbe\\qbi\\data\\osipi_dce_challenge'
os.environ["MADYM_ROOT"]='C:\\isbe\\code\\manchester_qbi\\public\\madym_cxx_builds\\msvc2019_no_gui\\bin\\Release'

#%% First unzip the complete data into the root folder
complete_data_zip = os.path.join(root_dir, 'osfstorage-archive.zip')
with zipfile.ZipFile(complete_data_zip,"r") as zip_ref:
    zip_ref.extractall(root_dir)

#%%
#Then unzip each of the clinical and synthetic datasets
for data_type in ['Clinical_Data']:#, 'Synthetic_Data']:
    data_dir = os.path.join(root_dir, data_type)

    dataset_zips = glob.glob(os.path.join(data_dir, '*.zip'))

    for dataset_zip in dataset_zips:
        zip_name = os.path.splitext(os.path.basename(dataset_zip))[0]
        target_dir = os.path.join(data_dir, zip_name)

        print(f'Unzipping {zip_name} to {target_dir}')
        with zipfile.ZipFile(dataset_zip,"r") as zip_ref:
            zip_ref.extractall(target_dir)
# %%

def make_brain_mask(visit_dir:str, thresh=30):
    
    #Load dynamic mean and threshold
    dyn_mean = nibabel.load(os.path.join(visit_dir, 'dynamic', 'dyn__mean.nii.gz'))
    brain_mask = dyn_mean.get_fdata() > thresh

    #Only keep largest connected component
    mask_labels = skm.label(brain_mask[:,:,:,0], connectivity=1)
    n_labels = np.max(mask_labels)
    label_sz = np.empty((n_labels))
    for label in range(n_labels):
        label_sz[label] = np.sum(mask_labels==label+1)
    max_label = np.argmax(label_sz)+1
    brain_mask = mask_labels == max_label

    #Convert mask array back to nifti image object
    brain_mask_ni = nibabel.Nifti1Image(
        brain_mask.astype(np.uint8), np.eye(4))
    brain_mask_ni.header['pixdim'] = dyn_mean.header['pixdim']

    #Save mask
    mask_dir = os.path.join(visit_dir, 'mask')
    os.makedirs(mask_dir, exist_ok=True)
    nibabel.save(brain_mask_ni, os.path.join(mask_dir, 'brain_mask.nii.gz'))
    return brain_mask




#%%
def get_dicom_dce_dir(visit_dir:str):
    dicom_dirs = glob.glob(os.path.join(visit_dir, '*BRAIN*'))
    for dicom_dir in dicom_dirs:
        n_series = len(next(os.walk(dicom_dir))[1])
        if n_series >= 7:
            break
    return os.path.basename(dicom_dir), n_series

# %%
from QbiMadym import madym_DicomConvert, madym_T1, madym_DCE

# %%
config_dir = os.path.join(root_dir, 'madym_config')

# %%

def analyse_patient_visit(config_dir, visit_dir: str, steps = [1,2,3,4]):
    '''
    Run complete analysis for a single patient visit, applying the following steps:
    1. Sort the DICOM data
    2. Convert the DICOM data to NIFTI volumes for Madym analysis
    3. Make ROI mask by thresholding temporal mean of DCE series
    4. Baseline T1 mapping of VFA data
    5. Fit the ETM to DCE data
    '''

    #Pre-check required for steps 1+2
    dicom_dce_dir,n_series = get_dicom_dce_dir(visit_dir)

    if (1 in steps) or (2 in steps):
        if n_series < 7:
            raise RuntimeError('Could not find data for DCE series')
        elif n_series == 7:
            t1_series = [1, 2, 3, 4, 5, 6]
            dyn_series = 7
        elif n_series == 8:
            t1_series = None
            dyn_series = None
        else:
            raise RuntimeError('Unexpected number of DCE series found')

    #1. Sort DICOM data
    if 1 in steps:
        madym_DicomConvert.run(
            config_file = os.path.join(config_dir, 'madym_dicom_sort_config.txt'),
            dicom_dir = dicom_dce_dir, 
            working_directory = visit_dir)

    #2. Convert the DICOM data to NIFTI volumes for Madym analysis
    if 2 in steps:
        madym_DicomConvert.run(
            config_file = os.path.join(config_dir, 'madym_dicom_make_config.txt'),
            dicom_dir = dicom_dce_dir, 
            T1_input_series = t1_series,
            dyn_series = dyn_series,
            working_directory = visit_dir)

    #3. Make ROI mask by thresholding temporal mean of DCE series
    if 3 in steps:
        make_brain_mask(visit_dir)

    #4. Baseline T1 mapping of VFA data
    if 4 in steps:
        madym_T1.run(
            config_file = os.path.join(config_dir, 'madym_T1_config.txt'), 
            working_directory = visit_dir)

    #5. Fit the ETM to DCE data
    if 5 in steps:
        madym_DCE.run()


# %%
num_patients = {
    'Clinical': 8,
    'Synthetic': 2 
    }
for dataset in ['Clinical', 'Synthetic']:
    for patient in range(1,num_patients[dataset]+1):
        for visit in  [1,2]:
            visit_dir = os.path.join(root_dir, 
                f'{dataset}_Data', 
                f'{dataset}_P{patient}', f'Visit{visit}')

            #Sanity check we've set the visit paths correctly
            if os.path.exists(visit_dir):
                print(f'Analysing {visit_dir}')
            else:
                print(f'Error {visit_dir} is missing: skipping to next')
                continue

            #Analyse the data
            analyse_patient_visit(config_dir, visit_dir, steps=[4])
# %%
#%%
%load_ext autoreload
%autoreload 2
#%%
import os
import numpy as np
import matplotlib.pyplot as plt
from scipy.interpolate import interp1d
from QbiPy.dce_models.dce_aif import Aif, AifType
from QbiMadym import madym_DCE
import nibabel as nib

os.environ["MADYM_ROOT"] = 'C:\\isbe\\code\\manchester_qbi\\public\\madym_cxx_builds\\msvc2019\\bin\\Release'

#%%
t_offset = 40
brain_aif = np.array(
    [
    [0,	-0.000146667],
    [7.6,	-0.000146667],
    [15.2,	-0.000293333],
    [22.8,	-1.66667E-05],
    [30.4,	0.00058],
    [38,	0.000186667],
    [45.6,	-1.33333E-05],
    [53.2,	0.00065],
    [60.8,	0.000236667],
    [68.4,	0.358256667],
    [76,	2.248876667],
    [83.6,	1.777016667],
    [91.2,	1.0866],
    [98.8,	1.04901],
    [106.4,	1.009556667],
    [114,	0.968133333],
    [121.6,	0.94308],
    [129.2,	0.921186667],
    [136.8,	0.896213333],
    [144.4,	0.871193333],
    [152,	0.845556667],
    [159.6,	0.820693333],
    [167.2,	0.798973333],
    [174.8,	0.77569],
    [182.4,	0.755873333],
    [190,	0.73949],
    [197.6,	0.728373333],
    [205.2,	0.71311],
    [212.8,	0.70215],
    [220.4,	0.69049],
    [228,	0.678706667],
    [235.6,	0.66934],
    [243.2,	0.659986667],
    [250.8,	0.646113333],
    [258.4,	0.640423333],
    [266,	0.634676667],
    [273.6,	0.630126667],
    [281.2,	0.62468],
    [288.8,	0.616346667],
    [296.4,	0.608636667],
    [304,	0.60261],
    [311.6,	0.597993333],
    [319.2,	0.588626667],
    [326.8,	0.586083333],
    [334.4,	0.580786667],
    [342,	0.575926667],
    [349.6,	0.572873333],
    [357.2,	0.5666]])


osipi_times = np.linspace(0, 4.8*64, 65)
aif_interp = interp1d(brain_aif[:,0]-t_offset, brain_aif[:,1], 
    kind='linear', fill_value='extrapolate', assume_sorted=True)   

resampled_AIF = aif_interp(osipi_times)

# %%
plt.figure()
plt.plot(brain_aif[:,0]-t_offset, brain_aif[:,1])
plt.plot(osipi_times, resampled_AIF, 'r.')
# %%
parker_AIF = Aif(times = osipi_times/60, prebolus=7, hct=0, dose=0.1)

#%%
plt.figure()
plt.plot(brain_aif[:,0]-t_offset, brain_aif[:,1])
plt.plot(osipi_times, resampled_AIF, 'r.')
plt.plot(osipi_times, parker_AIF.resample_AIF(0)[0,], 'g.')



# %%
aif_path = 'C:\\isbe\\qbi\\data\\osipi_dce_challenge\\Synthetic_Data\\Synthetic_P1\\Visit1\\madym_output\\AIF\\slice_9_Auto_AIF.txt'
auto_aif = Aif(aif_type = AifType.FILE, filename=aif_path)
#%%
plt.figure()
plt.plot(osipi_times, auto_aif.resample_AIF(0)[0,], 'g-')
plt.plot(osipi_times, resampled_AIF, 'r--')
plt.plot(osipi_times, parker_AIF.resample_AIF(0)[0,], 'b--')
# %%
os.environ["MADYM_ROOT"] = "C:\\Program Files\\Madym\\madym-v4.16.0\\bin"


#%%
root_dir = 'C:\\isbe\\qbi\\data\\osipi_dce_challenge'
visit_dir = os.path.join(root_dir, 'Clinical_Data', 'Clinical_P6', 'Visit1')
config_dir = os.path.abspath(os.path.join('..', 'madym_config'))
#%%

BATs = np.linspace(0, 9.6, 3)/60

for i_B, BAT in enumerate(BATs):
    #Call Madym to run the ETM fit for this BAT
    madym_DCE.run(
      config_file = os.path.join(config_dir, 'madym_ETM_config.txt'),
      audit_dir = os.path.join(visit_dir, 'audit_logs'),
      init_params = [0, 0, 0, BAT],
      output_dir = f'_BAT{i_B:02d}', 
      working_directory = visit_dir)
# %%
#%%
mdm_dir = os.path.join(visit_dir, 'madym_output', 'ETM_debug')

ktrans = nib.load(os.path.join(mdm_dir, 'Ktrans.nii.gz')).get_fdata()
ve = nib.load(os.path.join(mdm_dir, 'v_e.nii.gz')).get_fdata()
vp = nib.load(os.path.join(mdm_dir, 'v_p.nii.gz')).get_fdata()
#%%
plt.figure(figsize=(10,10))
plt.imshow(ktrans[:,:,8,0])
#%%
def get_dyn_vals(root_path, num_vols, roi, index_fmt = '01d', img_ext = '.hdr'):
    '''GET_DYN_VALS given directory of volumes and ROI mask, get array of
    time-series for voxels in the ROI
    [times] = get_dyn_vals(root_path, num_vols, roi, index_fmt)

    Parameters:
        root_path - folder + filename root where volumes are, or 4D array of loaded volumes

        num_vols - number of volumes to load

        roi - mask volume, must have same dims as dynamic volumes

        index_fmt ('01d') - format that converts indexes into volume suffix


    Returns:
        dyn_signals - N_vox x N_times array of time-series for each voxel
      '''

    #If ROI is a path, load it from disk
    if type(roi) == str:
        roi = nib.load(roi).get_fdata() > 0


    num_pts = np.count_nonzero(roi)
    dyn_signals = np.empty((num_pts, num_vols))

    load_vols = type(root_path) == str
    for i_vol in range(num_vols):
        if load_vols:
            vol_path = f"{root_path}{i_vol+1:{index_fmt}}{img_ext}"
            vol = nib.load(vol_path).get_fdata()
        else:
            vol = root_path[:,:,:,i_vol]
        
        dyn_signals[:,i_vol] = vol[roi]

    return dyn_signals
#%%
roi = (ktrans > 0.05) & (ve > 0) & (vp > 0) & (ve + vp < 1) 
Ct_s = get_dyn_vals(os.path.join(mdm_dir, 'Ct_sig', 'Ct_sig'), 65, roi, '02d', '.nii.gz')
Ct_m = get_dyn_vals(os.path.join(mdm_dir, 'Ct_mod', 'Ct_mod'), 65, roi, '02d', '.nii.gz')

# %%
for i_vox in range(10):
    plt.figure(figsize=(10,10))
    plt.plot(Ct_s[i_vox+0,:], 'b--')
    plt.plot(Ct_m[i_vox+0,:], 'r')
# %%
aif_path = 'C:\\isbe\\qbi\\data\\osipi_dce_challenge\\Synthetic_Data\\Synthetic_P1\\Visit1\\madym_output\\ETM_debug\\AIF.txt'
pop_aif = Aif(aif_type = AifType.FILE, filename=aif_path)
# %%

# %%
from analyse_data import analyse_patient_visit
#%%
analyse_patient_visit(visit_dir, config_dir)

# %%
mdm_dir = os.path.join(visit_dir, 'madym_output', 'ETM_final')

ktrans = np.squeeze(nib.load(os.path.join(mdm_dir, 'Ktrans.nii.gz')).get_fdata())
ve = np.squeeze(nib.load(os.path.join(mdm_dir, 'v_e.nii.gz')).get_fdata())
vp = np.squeeze(nib.load(os.path.join(mdm_dir, 'v_p.nii.gz')).get_fdata())
tau_a = np.squeeze(nib.load(os.path.join(mdm_dir, 'tau_a.nii.gz')).get_fdata())
#%%
plt.figure(figsize=(10,10))
plt.imshow(ktrans[:,:,8], vmin = 0, vmax=0.1)

plt.figure(figsize=(10,10))
plt.imshow(ve[:,:,8], vmin = 0, vmax=1.0)

plt.figure(figsize=(10,10))
plt.imshow(vp[:,:,8], vmin = 0, vmax=1.0)
#%%
plt.figure(figsize=(10,10))
plt.imshow(tau_a[:,:,8])
# %%
mask = np.squeeze(nib.load(os.path.join(visit_dir, 'mask', 'brain_mask.nii.gz')).get_fdata()) > 0
# %%
print(np.median(ktrans[mask]))
print(np.median(ve[mask]))
print(np.median(vp[mask]))
# %%
plt.figure()
plt.hist(tau_a[mask])
# %%
from zip_up_data import zipup_patient_visit

ktrans_dir = os.path.join(root_dir, 'OSIPI_DCE_challenge_QBI_Manchester')

zipup_patient_visit(visit_dir, ktrans_dir)
# %%
