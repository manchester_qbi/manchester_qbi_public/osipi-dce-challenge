#%%
import matplotlib.pyplot as plt
import nibabel as nib
import os
import numpy as np

#%%
def tile_map(map:np.ndarray, vmax = 0):
    '''
    The volumes in the osipi challenge are all 256x256x16, which
    tiles nicely, 4x4 slices to a 1024x1024 image
    '''
    tiled_map = np.empty((1024,1024))
    for slice in range(16):
        col = 256*(slice % 4)
        row = 256*(slice // 4)

        tiled_map[row:row+256,col:col+256] = np.squeeze(map[:,:,slice])

    if vmax:
        tiled_map /= vmax
        tiled_map[tiled_map < 0] = 0
        tiled_map[tiled_map > 1] = 1
        tiled_map *= 256

    return tiled_map

def load_map(visit_dir:str, map_pth:str):
    map = nib.load(os.path.join(visit_dir, map_pth+".nii.gz")).get_fdata()
    map_name = os.path.splitext(os.path.basename(map_pth))[0]
    return map, map_name

def view_map_by_slice(visit_dir:str, map_pth:str, slices=range(16)):
    map, map_name = load_map(visit_dir, map_pth)
    for i_z in slices:
        plt.figure(figsize=(10,8))
        plt.imshow(map[:,:,i_z])
        plt.colorbar()
        plt.title(f'{map_name} slice {i_z+1}')

def view_map_tiled(visit_dir:str, map_pth:str, vmax = 0):
    map, map_name = load_map(visit_dir, map_pth)
    tiled_map = tile_map(map, vmax)
    plt.figure(figsize=(12,10))
    plt.imshow(tiled_map, vmin = 0)
    plt.colorbar()
    plt.title(f'{visit_dir}, {map_name} all slices')

def save_map_tiled(visit_dir:str, map_pth:str, vmax = 0):
    map, map_name = load_map(visit_dir, map_pth)
    tiled_map = tile_map(map, vmax)
    save_name = os.path.join(visit_dir, map_pth+".png")
    print(save_name)
    plt.imsave(save_name, tiled_map)

#%%
root_dir = os.path.join('C:\\', 'isbe', 'qbi', 'data', 'osipi_dce_challenge')
mask_pth = os.path.join('mask', 'brain_mask')
T1_pth = os.path.join('madym_output', 'T1', 'T1')
Ktrans_pth = os.path.join('madym_output', 'ETM_final', 'Ktrans')
#%%
num_patients = {
    'Clinical': 8,
    'Synthetic': 2 
    }
for dataset in ['Clinical', 'Synthetic']:
    for patient in range(1,num_patients[dataset]+1):
        for visit in  [1,2]:
            visit_dir = os.path.join(root_dir, 
                f'{dataset}_Data', 
                f'{dataset}_P{patient}', f'Visit{visit}')

            save_map_tiled(visit_dir, mask_pth)
            save_map_tiled(visit_dir, T1_pth, 2000)
            save_map_tiled(visit_dir, Ktrans_pth, 0.1)

            if patient == 1 and visit == 1:
                view_map_tiled(visit_dir, mask_pth)
                view_map_tiled(visit_dir, T1_pth, 2000)
                view_map_tiled(visit_dir, Ktrans_pth, 0.1)
#%%
def print_summary_stats(visit_dir, fid):
    dirs = os.path.normpath(visit_dir).split(os.path.sep)
    subject = dirs[-2]
    visit = dirs[-1][-1]

    mask_pth = os.path.join(visit_dir, 'mask', 'brain_mask.nii.gz')
    T1_pth = os.path.join(visit_dir, 'madym_output', 'T1', 'T1.nii.gz')
    Ktrans_pth = os.path.join(visit_dir, 'madym_output', 'ETM_final', 'Ktrans.nii.gz')

    mask = nib.load(mask_pth).get_fdata() > 0
    T1 = nib.load(T1_pth).get_fdata()
    Ktrans = nib.load(Ktrans_pth).get_fdata()

    T1_med = np.nanmedian(T1[mask])
    Ktrans_med = np.nanmedian(Ktrans[mask])
    print(f'{subject}, {visit}, {T1_med:5.4f}, {Ktrans_med:5.4f}\n', file=fid)

def save_summary_stats(root_dir, fid):
    print('Subject, Visit, T1 median, Ktrans median\n', file=fid)
    for dataset in ['Clinical', 'Synthetic']:
        for patient in range(1,num_patients[dataset]+1):
            for visit in  [1,2]:
                visit_dir = os.path.join(root_dir, 
                    f'{dataset}_Data', 
                    f'{dataset}_P{patient}', f'Visit{visit}')
                print_summary_stats(visit_dir, fid)

with open(os.path.join(root_dir, 'summary_stats.csv'), 'wt') as fid:
    save_summary_stats(root_dir, fid)




# %%
dataset = 'Synthetic'
patient = 2

Ktrans = []
mask = []
for visit in [1,2]:

    visit_dir = os.path.join(root_dir, 
                f'{dataset}_Data', 
                f'{dataset}_P{patient}', f'Visit{visit}')

    Ktrans_pth = os.path.join(visit_dir, 'madym_output', 'ETM_final', 'Ktrans.nii.gz')
    mask_pth = os.path.join(visit_dir, 'mask', 'brain_mask.nii.gz')

    Ktrans.append(nib.load(Ktrans_pth).get_fdata())
    mask.append(nib.load(mask_pth).get_fdata() > 0)
# %%
for i_z in range(16):
    plt.figure(figsize=(8,8))
    plt.imshow((Ktrans[0][:,:,i_z] < 0) & (Ktrans[1][:,:,i_z] < 0))
# %%
mask2 = mask[0] & mask[1]
plt.figure()
plt.plot(Ktrans[0][mask2], Ktrans[1][mask2], 'r.')
plt.axis('equal')
plt.xlim((-0.1,0.1))
plt.ylim((-0.1,0.1))
# %%
enhancing_pth = os.path.join(visit_dir, 'madym_output', 'ETM_BAT01', 'enhVox.nii.gz')
enhVox = nib.load(enhancing_pth).get_fdata() > 0
# %%
