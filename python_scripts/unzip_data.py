#Python core imports
import sys
import zipfile
import os
import glob

#---------------------------------------------------------------
#---------------------------------------------------------------
def unzip_data(root_dir:str):
    '''
    '''
    print(f'Unzipping data in {root_dir}')
    
    
    #First unzip the complete data into the root folder
    complete_data_zip = os.path.join(root_dir, 'osfstorage-archive.zip')

    if not os.path.exists(complete_data_zip):
        raise RuntimeError(
            f'Main data zip file {complete_data_zip} is missing: processing failed.\n'
            f' Check input data_dir ({root_dir}) is correct')

    with zipfile.ZipFile(complete_data_zip,"r") as zip_ref:
        zip_ref.extractall(root_dir)

    #Then unzip each of the clinical and synthetic datasets
    for data_type in ['Clinical_Data', 'Synthetic_Data']:
        data_dir = os.path.join(root_dir, data_type)

        dataset_zips = glob.glob(os.path.join(data_dir, '*.zip'))

        for dataset_zip in dataset_zips:
            zip_name = os.path.splitext(os.path.basename(dataset_zip))[0]
            target_dir = os.path.join(data_dir, zip_name)

            print(f'Unzipping {zip_name} to {target_dir}')
            with zipfile.ZipFile(dataset_zip,"r") as zip_ref:
                zip_ref.extractall(target_dir)

#---------------------------------------------------------------
#---------------------------------------------------------------
if __name__ == '__main__':
    root_dir = ''

    if len(sys.argv) > 1:
        root_dir = sys.argv[1]
    
    unzip_data(root_dir)

