from QbiMadym import utils

mdm_version = ''
try:
  mdm_version = utils.local_madym_version()
except:
  pass

print('********************************************************\n')

if mdm_version == 'v4.16.1':
  print(f'madym_osipi_challenge environment successfully set-up, running Madym version = {mdm_version}')
else:
  print('Error creating conda environment, please contact Michael Berks')


print('\n********************************************************')