#!/bin/bash

#Activate the environment
source activate madym_osipi_challenge

#Run python script to run the main analysis
python python_scripts/analyse_data.py "$1"