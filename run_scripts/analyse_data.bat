@echo OFF

::Activate the environment
call activate madym_osipi_challenge

::Run python script to run the main analysis for each subject/visit
python python_scripts/analyse_data.py "%~1"