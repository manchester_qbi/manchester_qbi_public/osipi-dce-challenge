@echo OFF

::Activate the environment
call activate madym_osipi_challenge

::Run python script to unzip the data
python python_scripts/unzip_data.py "%~1"

::Run python script to prepare the data for each subject/visit
python python_scripts/prepare_data.py "%~1"