#!/bin/bash

#Create conda environment, the brackets stop this shell from exiting
(conda env create -f conda_env.yml)

#Activate the environment
source activate madym_osipi_challenge

#Install the madym wrappers
cp -a "$MADYM_ROOT/../python" "QbiMadym" 
pip install "./QbiMadym"
rm -rf "./QbiMadym"

#Check everything has installed correctly
python python_scripts/test_madym_installation.py