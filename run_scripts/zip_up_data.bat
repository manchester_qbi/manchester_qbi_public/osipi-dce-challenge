@echo OFF

::Activate the environment
call activate madym_osipi_challenge

::Run python script to zip-up data for distribution
python python_scripts/zip_up_data.py "%~1"