@echo OFF

::Create conda environment from the yaml file
call conda env create -f conda_env.yml

::Activate the environment
call activate madym_osipi_challenge

::Install the madym wrappers
xcopy /s "%MADYM_ROOT%\..\python" "QbiMadym\"
pip install ".\QbiMadym"
rmdir "QbiMadym" /s/q

::Check everything has installed correctly
python python_scripts\test_madym_installation.py

::Run python script to unzip the data
python python_scripts/unzip_data.py "%~1"

::Run python script to prepare the data for each subject/visit
python python_scripts/prepare_data.py "%~1"

::Run python script to run the main analysis for each subject/visit
python python_scripts/analyse_data.py "%~1"

::Run python script to zip-up data for distribution
python python_scripts/zip_up_data.py "%~1"

