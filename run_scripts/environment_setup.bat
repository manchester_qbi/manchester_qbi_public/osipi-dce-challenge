@echo OFF

::Create conda environment
call conda env create -f conda_env.yml

::Activate the environment
call activate madym_osipi_challenge

::Install the madym wrappers
xcopy /s "%MADYM_ROOT%\..\python" "QbiMadym\"
pip install ".\QbiMadym"
rmdir "QbiMadym" /s/q

::Check everything has installed correctly
python python_scripts\test_madym_installation.py