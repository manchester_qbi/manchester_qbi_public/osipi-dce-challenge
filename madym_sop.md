# Madym

This document provides instructions on how to run [Madym](https://gitlab.com/manchester_qbi/manchester_qbi_public/madym_cxx/-/wikis/Home) to complete the [OSIPI DCE Challenge](https://drive.google.com/file/d/12POGgTitC-0jXNTSMIvKJkWWmjtfDJkr/view).

## Overview
Madym is an open source C++ toolkit for quantitative DCE-MRI analysis. To analyse the data in the OSIPI DCE-MRI challenge we make use of the python wrappers provided with the main toolkit to prepare the data and then schedule the main analysis steps. We assume the user has Anaconda python installed, and run the complete analysis in a dedicated conda environment.

The complete operating protocol requires:

- Installing Madym

- Downloading/cloning the [Madym OSIPI DCE Challenge repository](https://gitlab.com/manchester_qbi/manchester_qbi_public/osipi-dce-challenge)

- Running the environment setup script to create a new conda environment in which the analysis will run

- Running the data preparation script to 
  - Convert the Dicom data for each dataset into NIFTI images and Madym xtr files
  - Compute an approximate segmentation of the brain from background to provide ROI masks for the main analysis

- Running the data analysis script which performs the following steps for each dataset

  - Computing baseline T1 from the variable flip-angle images

  - Scheduling a set of extended-Tofts models fits at different bolus-arrival times, choosing the parameters with lowest fit residual at each voxel

- Running the data post-processing script to generate a zip file of Ktrans maps for each dataset

These steps are detailed further below.

## Installing Madym
Install Madym version 4.16.1, either using the pre-built binary installers for [Windows](https://manchester_qbi.gitlab.io/manchester_qbi_public/madym_binaries/windows/madym_v4.16.1-Windows-10.0.19042.exe), [Mac OS](https://manchester_qbi.gitlab.io/manchester_qbi_public/madym_binaries/mac_os11/madym_v4.16.1-Darwin-20.5.0-no_gui.dmg) and [Ubuntu 20+](https://manchester_qbi.gitlab.io/manchester_qbi_public/madym_binaries/ubuntu/madym_v4.16.1.deb), or by building from [source code](https://gitlab.com/manchester_qbi/manchester_qbi_public/madym_cxx/-/tree/release-4.16.1) using the [build instructions](https://gitlab.com/manchester_qbi/manchester_qbi_public/madym_cxx/-/wikis/build_instructions). **Do not** try to complete the challenge with an older version of Madym. We had to make some minor fixes to deal with the OSIPI challenge data and thus older versions will not successfully process all the OSIPI datasets.

Check the installation by seeing if the environment variable `MADYM_ROOT` has been set.

If using Windows, open a command window and run
> echo %MADYM_ROOT%

Or if using MacOS/Linux, open a new terminal and run
> echo $MADYM_ROOT

This should show the path to which your Madym tools have been installed. If for any reason your `MADYM_ROOT` is blank, you must fix this *before* continuing. To do this, set `MADYM_ROOT` as an environment variable manually. It should point to the `bin` directory of the installed/built Madym tools.

For example, if you've installed using the default options on Windows you would expect to see
```
> echo %MADYM_ROOT%
C:\Program Files\Madym\madym-v4.16.1\bin
```

## Setting up a new conda environment

Download/clone the [Madym OSIPI DCE Challenge repository](https://gitlab.com/manchester_qbi/manchester_qbi_public/osipi-dce-challenge).

We have provided shell scripts (`.sh`) for unix based systems (*eg* MacOS/Linux) and batch scripts (`.bat`) for Windows. The instructions below are for unix-based, but if using Windows, just substitute `.bat` and backslashes in each case (*eg* call `run_scripts\environment_setup.bat` instead of `./run_scripts/environment_setup.sh` *etc*)

Open a new command/terminal window and `cd` into the root directory of this repository.

Run 
> ./run_scripts/environment_setup.sh

Ensure the final output of the script reads:
> madym_osipi_challenge environment successfully set-up, running Madym version = v4.16.1

Keep this terminal open, and complete the remaining steps from the same directory. These are completed using the shell/bash scripts in the `run_scripts` folder, however it is important you stay in the root directory as paths called by the scripts are relative to the root.

## Prepare the data

This assumes you have downloaded the data as a zip file `osfstorage-archive.zip` into a folder with path `<data_dir>`. The path `<data_dir>` must be passed as input to the remaining run scripts. If you have already unzipped the data, you can comment out the python call to the unzip script in `run_scripts/prepare_data.sh` (line 7). The examples below assume `<data_dir>` is `~/data/osipi_dce_challenge`, but you should substitute in your own data path. Because we convert the DICOM data to NIFTI before processing, we require more disk space than processing the DICOM data correctly. The complete process requires approximately 11GB of disk space (including the original and any intermediate zip files), so please ensure you have this available.

Run
> ./run_scripts/prepare_data.sh ~/data/osipi_dce_challenge

For each subject/visit this will:

- Unzip the dataset into an appropriately named folder

- Call Madym's Dicom tool to convert the Dicom data into NIFTI images and Madym XTR files

- Generate an ROI mask of the brain region using the temporal average of the dynamic time-series images

The script will take approximately 30 minutes to run on all 20 subject/visits. During this time you will see lots of messages in the terminal window. This will include repeated warnings because the DICOM challenge data is missing some of the tags Madym expects to see in DCE-MRI data - these can be ignored. If the script completes successfully, the terminal window will be cleared, and a final message will read:

> OSIPI DCE-MRI challenge: data prepared successfully.

## Run the main analysis

Run 
> ./run_scripts/analyse_data.sh ~/data/osipi_dce_challenge

For each subject/visit this will:

- Call Madym's T1 mapping tool to compute baseline T1 from the variable flip-angle images

- Run a series of extended-Tofts models fits at different bolus-arrival times, choosing the parameters with lowest fit residual each voxel. Each fit will:

  - Convert the dynamic time-series to contrast-agent concentrations using:
    - The previously computed baseline T1
    - A recomputed M0 constant such that the mean pre-bolus concentrations equal zero (rather than using that which was calculated with baseline M0)

  - Fit an extended-Tofts model (computing Ktrans, Ve and Vp) with a Parker population AIF shifted to the given arrival time using a linear-least squares solver

  - If the fit provides an improved residual, save the parameters, otherwise the previous parameters are retained.

- The final parameter maps are written out as NIFTI images (together with Madym's program/audit logs)

The script will take approximately 60-90 minutes to run on all 20 subject/visits. Ensure the final output of the script reads:
> OSIPI DCE-MRI challenge: data analysed successfully.

## Post-processing
Run 
> ./run_scripts/zip_up_data.sh ~/data/osipi_dce_challenge

This will:

- Copy the `Ktrans.nii.gz` parameter map for each subject/visit into a new folder

- Zip up the folder into an archive file ready for distribution

Ensure the final output of the script reads:
```
OSIPI DCE-MRI challenge: data zipped up successfully.
The final zipped submission file is ~/data/osipi_dce_challenge/OSIPI_DCE_challenge_QBI_Manchester.zip
Challenge complete!
```

## Running the analysis on individual subject/visits
If you have already unzipped the data, and want to process indiviudal subject visits without using the full batch scripts, this can be achieved in a python interpreter, with the current directory set to the `python_scripts` folder of this repository as follows:

```
import os
from prepare_data import prepare_subject_visit
from analyse_data import analyse_subject_visit

visit_dir = '' #Set to path of subject/visit folder to process
config_dir = os.path.abspath(os.path.join('..', 'madym_config'))

prepare_subject_visit(visit_dir, config_dir) #Runs NIFTI conversion and mask generation
analyse_subject_visit(visit_dir, config_dir) #Runs baseline T1 and ETM fitting
```

The output Ktrans parameter map will be stored as `Ktrans.nii.gz` in a folder `madym_output\ETM_final` inside the visit folder.

## Running all steps in a single script
The complete analysis, starting from the initial zipped data file with Madym installed can be run using the single script:
> ./run_scripts/run_all_steps.sh <data_dir>

We recommend against this first time round because if anything goes wrong, it is a lot easier to understand where and how to fix by applying the analysis step-by-step. However, if you are happy to do so (*eg* to repeat the analysis when you already know it works), please use this script.









